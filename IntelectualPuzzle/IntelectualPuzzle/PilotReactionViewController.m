//
//  PilotReactionViewController.m
//  IntelectualPuzzle
//
//  Created by Timotin Vanea on 4/21/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "PilotReactionViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PilotReactionViewController ()
{
    NSArray *arrayPoints;
    NSMutableArray *arrayDirections;
    BOOL wasCollision;
    CGPoint d1,d2,d3,d4;
    double timeRecord;
}

@end

@implementation PilotReactionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    timeRecord = 0.0;
    viewWorkTable.layer.borderColor = [UIColor blackColor].CGColor;
    viewWorkTable.layer.borderWidth = (IS_IPAD)?30:10;
    
    CGPoint e1 = ((IS_IPAD)?CGPointMake(181, 181):CGPointMake(72, 72));
    CGPoint e2 = ((IS_IPAD)?CGPointMake(587, 138):CGPointMake(236, 56));
    CGPoint e3 = ((IS_IPAD)?CGPointMake(156, 622):CGPointMake(62, 250));
    CGPoint e4 = ((IS_IPAD)?CGPointMake(587, 620):CGPointMake(236, 250));
    CGPoint e5 = CGPointMake(viewWorkTable.frame.size.width/2, viewWorkTable.frame.size.height/2);
    arrayPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:e1],[NSValue valueWithCGPoint:e2],[NSValue valueWithCGPoint:e3],[NSValue valueWithCGPoint:e4],[NSValue valueWithCGPoint:e5], nil];
    
    [self setDirections];
    // Do any additional setup after loading the view.
    wasCollision = NO;
    
 
}

- (void) setDirections
{
    d1 = CGPointMake(1, 1);
    d2 = CGPointMake(-2, 0.5);
    d3 = CGPointMake(2, -3);
    d4 = CGPointMake(-0.5, -3);
}

- (void)start
{
    randomMainTimer = [NSTimer scheduledTimerWithTimeInterval:(0.01) target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    

}

- (void)onTimer
{
    timeRecord = timeRecord + 0.01;
    for (int i = 1; i <=4; i++) {
        CGPoint pointPosition = [self getPointForID:i];
    
        pointPosition = [self setVelocityForPoint:pointPosition];
    
        UIView *viewEnemy = [self.view viewWithTag:i];
        
        viewEnemy.center = CGPointMake(viewEnemy.center.x + pointPosition.x, viewEnemy.center.y + pointPosition.y);
        
        if (viewEnemy.center.x > viewWorkTable.frame.size.width-viewEnemy.frame.size.width/2 || viewEnemy.center.x < viewEnemy.frame.size.width/2) {
            pointPosition.x = -pointPosition.x;
        }
        
        if (viewEnemy.center.y > viewWorkTable.frame.size.height-viewEnemy.frame.size.height/2 || viewEnemy.center.y < viewEnemy.frame.size.height/2) {
            pointPosition.y = -pointPosition.y;
        }
        [self setPoint:pointPosition forID:i];
    }
    
    [self checkCollision];
    
}

- (CGPoint)setVelocityForPoint:(CGPoint)point
{
    float k = 0.0009*timeRecord;

    CGPoint rPoint;
    float x, y;
    if (point.x > 8.0 || point.x < -7.0)
    {
        x = point.x;
    }
    else
    {
        x =((point.x < 0)?point.x-k:point.x+k);
    }
    
    if (point.y > 7.0 || point.y < -8.0)
    {
        y = point.y;
    }
    else
    {
        y =((point.y < 0)?point.y-k:point.y+k);
    }
    rPoint = CGPointMake(x, y);
    return rPoint;
}

- (CGPoint)getPointForID:(int)idView
{
    
    CGPoint point;
    switch (idView) {
        case 1:
            point = d1;
            break;
        case 2:
            point = d2;
            break;
        case 3:
            point = d3;
            break;
        case 4:
            point = d4;
            break;
        default:
            break;
    }
    
    return point;
    
}

- (void)setPoint:(CGPoint)point forID:(int)idView
{
    switch (idView) {
        case 1:
            d1 = point;
            break;
        case 2:
            d2 = point;
            break;
        case 3:
            d3 = point;
            break;
        case 4:
            d4 = point;
            break;
        default:
            break;
    }
    

    
}


- (void)checkCollision
{
    for (int i = 1; i <=4; i++) {
        UIView *viewEnemy = [self.view viewWithTag:i];
        if (CGRectIntersectsRect(viewEnemy.frame, viewPlayer.frame)) {
            if (randomMainTimer.isValid) {
                [randomMainTimer invalidate];
                [self addAlert];
            }
            wasCollision = YES;
            [self setDirections];
        }
    }
    
    if (viewPlayer.center.x > viewWorkTable.frame.size.width-(viewPlayer.frame.size.width-viewPlayer.frame.size.width/3) || viewPlayer.center.x < (viewPlayer.frame.size.width-viewPlayer.frame.size.width/3)) {
        if (randomMainTimer.isValid) {
            [randomMainTimer invalidate];
            [self addAlert];
        }
        
        wasCollision = YES;
        [self setDirections];

    }
    
    if (viewPlayer.center.y > viewWorkTable.frame.size.height-(viewPlayer.frame.size.height-viewPlayer.frame.size.height/3) || viewPlayer.center.y < (viewPlayer.frame.size.height-viewPlayer.frame.size.height/3)) {
        if (randomMainTimer.isValid) {
            [randomMainTimer invalidate];
            [self addAlert];
        }
        
        wasCollision = YES;
        [self setDirections];
    }
    if (wasCollision) {
        for (int i = 1; i <=5; i++){
            UIView *view = [self.view viewWithTag:i];
            NSValue *value = [arrayPoints objectAtIndex:(i-1)];
            CGPoint enemyPointPos = [value CGPointValue];
            [view setCenter:enemyPointPos];
        }
    }
}
- (void)addAlert
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your Time:" message:[NSString stringWithFormat:@"%.3f",timeRecord] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!randomMainTimer.isValid) {
        timeRecord = 0.0;
        [self start];
        wasCollision = NO;
    }
    UITouch *playerTouch = [[event allTouches] anyObject];
    viewPlayer.center = [playerTouch locationInView:viewWorkTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//    // Ignoring specific orientations we don't care about.
//    if (orientation == UIDeviceOrientationFaceUp ||
//        orientation == UIDeviceOrientationFaceDown ||
//        orientation == UIDeviceOrientationUnknown) {
//        return;
//    }
//    // On orientation changes we do care about, remove the Smart Banner entry and
//    // replace it with the Smart Banner entry correct for the orientation we are
//    // in.
//    
//    if (UIInterfaceOrientationIsLandscape(orientation)) {
//        
//        viewWorkTable.frame = CGRectMake(0, 0, 700, 700);
//        viewWorkTable.center = CGPointMake(HEIGHT_DEVICE/2, WIDTH_DEVICE/2);
//        
//    } else if (UIInterfaceOrientationIsPortrait(orientation)) {
//        
//        viewWorkTable.frame = CGRectMake(0, 0, 764, 764);
//        viewWorkTable.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE/2);
//        
//    }
//    
//}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
