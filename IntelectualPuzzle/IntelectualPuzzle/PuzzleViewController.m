//
//  PuzzleViewController.m
//  IntelectualPuzzle
//
//  Created by Timotin Vanea on 12/12/13.
//  Copyright (c) 2013 Timotin Vanea. All rights reserved.
//

#import "PuzzleViewController.h"

@interface PuzzleViewController ()

@end

@implementation PuzzleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	

    
    // Do any additional setup after loading the view.
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageV.image = [UIImage imageNamed:@"arrow.png"];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    view.backgroundColor = [UIColor redColor];
    [view addSubview:imageV];
    view.tag = 1;
    [self.view addSubview:view];
    [self addGestureRecognizerForView:view];
    [view setMultipleTouchEnabled:YES];

    
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(200, 0, 200, 200)];
    view1.backgroundColor = [UIColor blueColor];
    [view1 addSubview:imageV];
    view1.tag = 2;
    [self.view addSubview:view1];
    [self addGestureRecognizerForView:view1];
    [view1 setMultipleTouchEnabled:YES];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void) addGestureRecognizerForView:(UIView*)vi{
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    [vi addGestureRecognizer:panRecognizer];
    panRecognizer.delegate=self;
   
    
    /*
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
    [vi addGestureRecognizer:pinchRecognizer];
    pinchRecognizer .delegate=self;
    */
    
    
    UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetected:)];
    [vi addGestureRecognizer:rotationRecognizer];
    rotationRecognizer.delegate=self;
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapRecognizer.numberOfTapsRequired = 2;
    [vi addGestureRecognizer:tapRecognizer];
    tapRecognizer.delegate = self;
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        for (UIView *thisView in self.view.subviews) {
            if (thisView.tag != piece.tag) {
                [self.view insertSubview:thisView belowSubview:piece];
            }
        }
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
        
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Gesture Recognizers Method

- (void)panDetected:(UIPanGestureRecognizer *)panRecognizer
{
    
    UIView *piece = [panRecognizer view];
    
    [self adjustAnchorPointForGestureRecognizer:panRecognizer];
    if ([panRecognizer state] == UIGestureRecognizerStateBegan || [panRecognizer state] == UIGestureRecognizerStateChanged) {
        CGPoint translation = [panRecognizer translationInView:[piece superview]];
        CGPoint imageViewPosition = piece.center;
        imageViewPosition.x += translation.x*0.5;
        imageViewPosition.y += translation.y*0.5;
        
        piece.center = imageViewPosition;
        [panRecognizer setTranslation:CGPointZero inView:[piece superview]];
    }
//    if(panRecognizer.state == UIGestureRecognizerStateEnded)
//    {
//        //All fingers are lifted.
//        
//        NSLog(@"%d", [self itIsSolved]);
//    }

}
#pragma mark - Pinch
/*
- (void)pinchDetected:(UIPinchGestureRecognizer *)pinchRecognizer
{
    UIView *piece = [pinchRecognizer view];
    
    [self adjustAnchorPointForGestureRecognizer:pinchRecognizer];
    
    CGFloat scale = pinchRecognizer.scale;
    piece.transform = CGAffineTransformScale(piece.transform, scale, scale);

    pinchRecognizer.scale = 1.0;
}
*/

- (void)rotationDetected:(UIRotationGestureRecognizer *)rotationRecognizer
{
    UIView *piece = [rotationRecognizer view];
    [self adjustAnchorPointForGestureRecognizer:rotationRecognizer];
    
    if ([rotationRecognizer state] == UIGestureRecognizerStateBegan || [rotationRecognizer state] == UIGestureRecognizerStateChanged) {
        CGFloat angle = rotationRecognizer.rotation*0.3;
        
        piece.transform = CGAffineTransformRotate(piece.transform, angle);
        
        rotationRecognizer.rotation = 0.0;
    }
}

- (void)tapDetected:(UITapGestureRecognizer *)tapRecognizer
{
    UIView *piece = [tapRecognizer view];
    
    [self adjustAnchorPointForGestureRecognizer:tapRecognizer];
    for (UIImageView *img in piece.subviews) {
            img.transform = CGAffineTransformScale(img.transform, -1, 1);
    }
    
//    UIView *piece = [tapRecognizer view];
//    [self adjustAnchorPointForGestureRecognizer:tapRecognizer];
    
    
//    [UIView animateWithDuration:0.25 animations:^{
//        self.imageView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
//        self.imageView.transform = CGAffineTransformIdentity;
//    }];
    
}

#pragma mark - Delegate Method

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"end %d",[self itIsSolved]);
    NSLog(@"piece 1 %@", NSStringFromCGPoint([self.view viewWithTag:1].center));
    NSLog(@"piece 2 %@", NSStringFromCGPoint([self.view viewWithTag:2].center));
}


#pragma mark - Control

- (BOOL) itIsSolved {
//    for (int i = 0; i <= 2; i++) {
    UIView *view1 = [self.view viewWithTag:1];
    UIView *view2 = [self.view viewWithTag:2];
    NSLog(@"%.2f = %.2f",view1.center.y, view2.center.y);
        if (view1.center.y <= view2.center.y+20  && view1.center.y >= view2.center.y-20 ) {
            NSLog (@"este la acest nivel");
            if (fabs((view2.center.x - view1.center.x)) + 20 >= 200 && fabs((view2.center.x - view1.center.x)) - 20 <= 200) {
                NSLog(@"BUN  %.2f",fabs(view2.center.x - view1.center.x));
                return YES;
            }
            
        }
//    }
    NSLog(@"----- %.2f", fabs(view2.center.x - view1.center.x));
    return NO;
}
@end
