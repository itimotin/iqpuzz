//
//  PilotReactionViewController.h
//  IntelectualPuzzle
//
//  Created by Timotin Vanea on 4/21/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PilotReactionViewController : UIViewController
{
    IBOutlet UIView *viewWorkTable;
    NSTimer *randomMainTimer;
    IBOutlet UIView *viewPlayer;
    
}
- (IBAction)back:(id)sender;

@end
