//
//  AppDelegate.h
//  IntelectualPuzzle
//
//  Created by Timotin Vanea on 12/12/13.
//  Copyright (c) 2013 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
